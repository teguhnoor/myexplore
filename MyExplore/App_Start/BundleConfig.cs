﻿using System.Web;
using System.Web.Optimization;

namespace MyExplore
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //------------- Myexplore Intro Section ----------------//
            bundles.Add(new StyleBundle("~/Content/ExploreIntroCss").Include(
                    "~/Content/Explore/dist/css/bootstrap.css",
                    "~/Content/Explore/assets/css/custom.css",
                    "~/Content/Explore/assets/css/intro.css",
                    "~/Content/Explore/assets/css/font-awesome.css",
                    "~/Content/Explore/examples/carousel/carousel.css",
                    "~/Content/Explore/css/fullscreen.css",
                    "~/Content/Explore/rs-plugin/css/settings.css",
                    "~/Content/Explore/assets/css/jquery-ui.css"
                ));

            bundles.Add(new ScriptBundle("~/Content/ExploreIntroJquery").Include(
                    "~/Content/Explore/assets/css/jquery.v2.0.3.js",
                    "~/Content/Explore/dist/js/bootstrap.min.js",
                    "~/Content/Explore/assets/js/js-intro2.js",
                    "~/Content/Explore/assets/js/jquery.preload.js",
                    "~/Content/Explore/assets/js/jquery.easing.js",
                    "~/Content/Explore/assets/js/jquery.nicescroll.min.js",
                    "~/Content/Explore/assets/js/jquery-ui.js",
                    "~/Content/Explore/assets/js/jquery.customSelect.js",
                    "~/Content/Explore/assets/js/functions.js",
                    "~/Content/Explore/assets/js/jquery.carouFredSel-6.2.1-packed.js",
                    "~/Content/Explore/assets/js/helper-plugins/jquery.touchSwipe.min.js",
                    "~/Content/Explore/assets/js/helper-plugins/jquery.mousewheel.min.js",
                    "~/Content/Explore/assets/js/helper-plugins/jquery.transit.min.js",
                    "~/Content/Explore/assets/js/helper-plugins/jquery.ba-throttle-debounce.min.js"
                ));

            //------------- End Of Intro Section ------------------//

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
