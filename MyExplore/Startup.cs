﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyExplore.Startup))]
namespace MyExplore
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
