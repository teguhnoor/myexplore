﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyExplore.Controllers
{
    public class MyExploreController : Controller
    {
        // GET: MyExplore
        public ActionResult Intro()
        {
            return View();
        }
        public ActionResult Home()
        {
            return View();
        }
        public ActionResult Hotel()
        {
            return View();
        }
        public ActionResult Vacations()
        {
            return View();
        }
        public ActionResult ThingsToDo()
        {
            return View();
        }
        public ActionResult MyAccount()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Register()
        {
            return View();
        }
        public ActionResult PlanTrip()
        {
            return View();
        }
        public ActionResult Details()
        {
            return View();
        }
    }
}